FROM ubuntu:focal
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

# If running this image with an user other than "user (uid 1000)" or "root" be sure to set your home to /home/user (export HOME=/home/user)

# Android tools versions: https://developer.android.com/studio/releases/platform-tools
# Android platform versions: https://developer.android.com/studio/releases/platforms
# Gradle versions: https://gradle.org/releases/

ENV GradleVersion="7.0" \
    NodeVersion="16" \
    AndroidVersions="platforms;android-28 platforms;android-29 platforms;android-30" \
    AndroidToolsVersion="30.0.2" \
    ANDROID_HOME="/android" \
    ANDROID_SDK_ROOT="/android" \
    JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre" \
    PATH="$PATH:/android/tools:/android/tools/bin:/opt/gradle/bin:/android/build-tools/29.0.0" \
    DEBIAN_FRONTEND="noninteractive" \
    TERM="xterm" \
    CDK_HOME=/home/user

ADD scripts/start-vscode.sh /start-vscode.sh

# Installing packages
RUN apt-get update \
	&& printf "\n\033[0;36mInstalling base packages...\033[0m\n" \
    && apt-get install --yes \
        sudo \
        apt-utils \
        locales \
        software-properties-common \
        zip \
        unzip \
        gnupg2 \
        git \
        curl \
        wget \
        openjdk-8-jdk \
        gcc \
        g++ \
        make \
        whiptail \
        cpio \
        wine64 \
        jq \
# Initial configuration
    && printf "\n\033[0;36mInitial configuration...\033[0m\n" \
    && locale-gen en_US.UTF-8 \
    && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 \
    && export LANG=en_US.UTF-8 \
# Node.js
    && printf "\n\033[0;36mInstalling Node.js...\033[0m\n" \
    && curl -sL https://deb.nodesource.com/setup_$NodeVersion.x | bash - \
    && apt-get update \
    && apt-get install --yes nodejs \
    && npm install -g npm@latest \
    && npm --version \
# Yarn
    && curl -s https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo 'deb https://dl.yarnpkg.com/debian/ stable main' > /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install --yes yarn \
# Ionic, Cordova and other node packages
    && printf "\n\033[0;36mInstalling Ionic, Cordova, ...\033[0m\n" \
    && npm install -g --unsafe-perm=true --allow-root @ionic/cli @angular/cli cordova cordova-res native-run eslint tslint typescript license-checker \
# Telemetry off
    && ng analytics off \
    && cordova telemetry off \
    && ionic config set -g telemetry false \
# Gradle
    && printf "\n\033[0;36mInstalling Gradle...\033[0m\n" \
    && echo "Downloading Gradle..." && wget --progress=dot -O /tmp/gradle.zip https://services.gradle.org/distributions/gradle-$GradleVersion-bin.zip 2>&1 | grep --line-buffered "%" | sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}' && echo "" \
    && unzip -q -d /tmp/gradle /tmp/gradle.zip \
    && mv /tmp/gradle/gradle-$GradleVersion /opt/gradle \
# Android
    && printf "\n\033[0;36mInstalling Android...\033[0m\n" \
    && echo "Downloading Android Tools..." && wget --progress=dot -O /tmp/android.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip 2>&1 | grep --line-buffered "%" | sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}' && echo "" \
    && unzip -q -d "${ANDROID_HOME}" /tmp/android.zip \
    && mkdir -p ~/.android \
    && touch ~/.android/repositories.cfg \
    && yes | sdkmanager --licenses >/dev/null \
    && sdkmanager "platform-tools" "build-tools;$AndroidToolsVersion" $AndroidVersions >/dev/null \
    && chmod -R 0777 "${ANDROID_HOME}" \
# Add user
    && addgroup --gid 1000 user \
    && adduser --disabled-password --gecos '' --uid 1000 --gid 1000 user \
    && usermod -aG sudo user \
    && echo "Set disable_coredump false" >> /etc/sudo.conf \
    && echo "user ALL=(ALL:ALL) ALL" > /etc/sudoers \
    && echo "%user ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers \
    && chmod -R 0777 /home/user \
# Environmental preparations
    && printf "\n\033[0;36mPreparing environment...\033[0m\n" \
    mkdir -p /.electron-gyp \
    export npm_config_loglevel=verbose \
# Visual Studio Code
    && mkdir -p /vscode/data /vscode/extensions \
    && chmod -R 0777 /vscode \
    && curl -fsSL https://code-server.dev/install.sh | sh \
    && chmod 0777 /start-vscode.sh \
# Various preparations
    && mkdir -p /wd \
    && mkdir /info && touch /info/IsDocker && chmod -R 0755 /info \
    && cat /etc/os-release | grep VERSION_ID | cut -d "\"" -f 2 > /info/ubuntu \
    && node --version | cut -d "v" -f 2 > /info/node \
    && npm --version > /info/npm \
    && ionic --version > /info/ionic \
    && sdkmanager --list | grep build-tools/ | cut -d "/" -f 2 | tr '\n' '-' | tr -d '[:space:]' | sed 's/.$//' > /info/android-build-tools \
    && sdkmanager --list | grep platform-tools/ | cut -d "|" -f 2 | tr '\n' '-' | tr -d '[:space:]' | sed 's/.$//' > /info/android-platform-tools \
    && sdkmanager --list | grep platforms/android | cut -d "|" -f 1 | cut -d "-" -f 2 | tr '\n' '-' | tr -d '[:space:]' | sed 's/.$//' > /info/android-platform \
    && echo "$GradleVersion" > /info/gradle \
# Cleanup
    && printf "\n\033[0;36mCleaning up...\033[0m\n" \
    && npm cache clean --force \
    && rm -r /tmp/* \
    && apt-get clean all \
    && rm -rf /var/lib/apt/lists/*

USER user

# Test Ionic and Android build
RUN printf "\n\033[0;36mTesting Ionic...\033[0m\n" \
    && cd /tmp \
    && ionic start Ionic5 blank --capacitor --no-git --no-link --type=angular --project-id=ionic5 --package-id=com.cyancor.ionic5 \
    && cd ionic5 \
    && npm install @capacitor/android \
    && ionic build --prod \
    && npx cap add android \
    && npx cap sync \
    && npx cap update android \
    && cd android \
    && gradle wrapper \
    && ./gradlew check \
    && ./gradlew assembleRelease \
    && rm -r /tmp/* \
# Done
    && printf "\n\033[0;36mDone.\033[0m\n"
